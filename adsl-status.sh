#!/usr/bin/perl

# DETAILED ABOUT:
# x) connect to our ADSL router with curl and pull down the connection stats
# x) strip out the html etc and make the data into nice perl variables
# x) pull max uptime, maxspeed, average speed from sql
# x) detect adsl reset events (current_uptime < previous_uptime) 
# x) compare current stats with maximum stats from SQL to alert if we have a new record!
# x) squirt all the newly gathered data into mysql
# x) finally print out some useful stats
# x) plus:
# x) if run with --reset connect to our ADSL router with curl and reset the adsl connection
# x) if run with --log show the last 5 rows, or if --log X, show the last X rows.
# x) if run with --verbose run as per --log plus some more detailed info on speed etc.
# x) if run with --debug then output lots of intermediary debug crap blah etc
#
# POSSIBLE ISSUES, MORE TESTING NEEDED: 
# x) It wont create an error if i pass an invalid argument.
# x) If run sucessively reset_detection flags multiple false positives after a single reset event.
#     I think that might be because there are multiple means of calculating previous_uptime_in_seconds
#     and the results dont seem to agree.. ie:
#~      $ ./adsl.sync.dev.sh --debug | grep previous_uptime_in_seconds
#~      debug:   sub_get_previous_uptime(); previous_uptime_in_seconds=27919
#~      debug:   sub_get_previous_uptime(); previous_uptime_in_seconds=27919
#~      debug:   sub_detect_reset_event(): previous_uptime_in_seconds=27919     
#
# ACTUAL PROPER BUGS:
# x) It won't error if if i pass in erroneous argument to the script
#-------------------------------------------------------------------------------------------------------

# USE ONE ZILLION PERL MODULES
use Getopt::Long;
use strict;
use File::Spec;
use HTML::Strip;
use POSIX qw(ceil);
use Math::Round;
use Term::ANSIColor;
use Term::ANSIColor qw(:constants);
$Term::ANSIColor::AUTORESET = 1;
use DBI;
use DBD::mysql;

# DECLARE INCOMING ARGUMENTS USING GETOPT::LONG
my ($help, $verbose, $debug, $reset, $silent, $log, $fullog, $backupdb,$about);

# MYSQL CONFIG VARIABLES
my $database = "adsldb";
my $user = "adsl";
my $pw = "adslpass";
my $dsn = "dbi:mysql:$database:localhost:3306";

main();

#-------------------------------------------------------------------------------------------------------
sub sub_general_about(){
	print "Talk to the adsl router and mysql to record and display stats regarding adsl modem connection. Eg:\n\n";
	print " 	  ADSL2 synced @ 5816/1037kbs ~ 710KB/s (max 710KB/s NR!). Up 3 day 5 hr 21 min 54 sec (4642 mins, max 4640 NR!).\n";
    print "         ^             ^     ^         ^             ^     ^                  ^                ^              ^   ^\n";
	print "         |             |     |         |             |     |                  |                |              |   |\n";
	print "         |             |     |         |             |     |                  |                |              |  an uptime record!\n";
	print "         |             |     |         |             |     |                  |                |     Max ever uptime in mins\n";
	print "         |             |     |         |             |     |                  |       Current uptime in mins\n";
	print "         |             |     |         |             |     |    Current uptime in plain english\n";
	print "         |             |     |         |             |  a New downstream speed Record!! \n";
	print "         |             |     |         |   Our fastest ever downstream KB/s\n";
	print "         |             |     |   Downstream kilobytes p/sec.\n";
	print "         |      Downstream & upstream kilobits p/sec.\n";
	print "  What kind of ADSL connection was negotiated.\n\n";
}

sub main {
	# PARSE INCOMING ARGUMENTS USING GETOPT::LONG
	#sub_usage() if ( @ARGV < 1 or
	sub_usage() if (! GetOptions('help|?' => \$help,
								'verbose' => \$verbose,
								'about' => \$about,
								'silent' => \$silent,
								'debug' => \$debug,
								'log' => \$log,
								'fullog' => \$fullog,								
								'backupdb' => \$backupdb,
								'reset' => \$reset)
			or defined $help );    
			
	if ($debug) {
		sub_stats_this_script();
		print "debug: incoming arguments are help=$help, verbose=$verbose, debug=$debug, reset=$reset\n";
		if ($silent) { print "debug: --silent operation requested\n";}
	}
			
	# DO ONE OF THE OTHER FUNCTIONS
	if ($reset) {sub_reset_adsl_connection();exit;}
	if ($log) {sub_get_resetslog_from_mysql();exit;}
	if ($fullog) {sub_get_fulllog_from_mysql();exit;}
	if ($about) {sub_general_about();exit;}	
	if ($backupdb) {sub_backup_database();exit;}
	
	# OR THE MAIN DEAL
	sub_adsl_stats(); 		
}
	
sub sub_usage {
	if ($debug) {print "debug: entering sub_usage()\n";}
	print "Unknown option: @_\n" if ( @_ );
	print "usage: adsl.sync[.dev].sh [--verbose|--silent|--help|--debug] | [--reset]\n";
	exit;
}          

sub sub_insert_new_stats_into_mysql($ $ $ $ $ $ $) {
	if ($debug) {print "debug: entering sub_insert_new_stats_into_mysql()\n";}

	#~ mysql> show columns from manual_log;
	#~ +-------------+-----------+------+-----+-------------------+-----------------------------+
	#~ | Field       | Type      | Null | Key | Default           | Extra                       |
	#~ +-------------+-----------+------+-----+-------------------+-----------------------------+
	#~ | id          | int(11)   | NO   | PRI | NULL              | auto_increment              |
	#~ | ts          | timestamp | NO   |     | CURRENT_TIMESTAMP | on update CURRENT_TIMESTAMP |
	#~ | bps_down    | int(11)   | YES  |     | NULL              |                             |
	#~ | bps_up      | int(11)   | YES  |     | NULL              |                             |
	#~ | KBs         | int(11)   | YES  |     | NULL              |                             |
	#~ | uptime      | int(11)   | YES  |     | NULL              |                             |
	#~ | resetted    | char(11)  | YES  |     | NULL              |                             |
	#~ | prev_uptime | int(11)   | YES  |     | NULL              |                             |
	#~ | prev_KBs    | int(11)   | YES  |     | NULL              |                             |
	#~ +-------------+-----------+------+-----+-------------------+-----------------------------+
	#~ 9 rows in set (0.00 sec)
	
	# PARSE THE ARGUMENTS AND MAP THEM TO VARIABLES READY FOR SQL STATEMENT
	local(my $new_bps_down=$_[0]);
	local(my $new_bps_up=$_[1]);
	local(my $new_kb=$_[2]);
	local(my $new_uptime=$_[3]);	
	local(my $reset_event_detected=$_[4]);	
	local(my $previous_uptime_in_seconds=$_[5]);	
	local(my $previous_KBs=$_[6]);	
		
	# INSERT SOME NEW DATA
	my $connect = DBI->connect($dsn, $user, $pw);
	my $query = "INSERT into manual_log (bps_down, bps_up, KBs, uptime, resetted,prev_uptime,prev_KBs) VALUES('$new_bps_down','$new_bps_up','$new_kb','$new_uptime','$reset_event_detected','$previous_uptime_in_seconds','$previous_KBs');";
	
	if ($debug) {print "debug:   query = $query\n";}
	my $query_handle = $connect->prepare($query);
	$query_handle->execute();		
}

sub sub_get_maxuptime_from_mysql() {
	if ($debug) {print "debug: entering sub_get_maxuptime_from_mysql()\n";}
	my $connect = DBI->connect($dsn, $user, $pw);

	# SELECT MAX uptime as $max_uptime_ts
	my $query = "SELECT ts, max(uptime) FROM manual_log";	my $query_handle = $connect->prepare($query);	$query_handle->execute();	$query_handle->bind_columns(\my $max_uptime_ts, \my $max_uptime);	$query_handle->fetch();
	
	if ($debug) {print "debug:   query=$query\n";}	
	if ($debug) {print "debug:   max_uptime = $max_uptime\n";}
		
	return $max_uptime;
}

sub sub_get_average_uptime_from_mysql() {
	if ($debug) {print "debug: entering sub_get_average_uptime_from_mysql()\n";}
	my $connect = DBI->connect($dsn, $user, $pw);

	# SELECT MAX uptime as $avg_uptime_in_seconds
	my $query = "SELECT avg(prev_uptime) FROM manual_log where resetted='true'";	my $query_handle = $connect->prepare($query);	$query_handle->execute();	$query_handle->bind_columns(\my $avg_uptime_in_seconds);	$query_handle->fetch();
	#my $query = "SELECT avg(uptime) FROM manual_log";	my $query_handle = $connect->prepare($query);	$query_handle->execute();	$query_handle->bind_columns(\my $avg_uptime_in_seconds);	$query_handle->fetch();
	
	if ($debug) {print "debug:   query=$query\n";}
	if ($debug) {print "debug:   avg_uptime_in_seconds = $avg_uptime_in_seconds\n";}
	
	return $avg_uptime_in_seconds;
}

sub sub_get_average_speed_from_mysql() {
	if ($debug) {print "debug: entering sub_get_average_speed_from_mysql()\n";}
	
	my $connect = DBI->connect($dsn, $user, $pw);

	# SELECT average KBs as $average_down_KBs
	my $query = "select avg(KBs) FROM manual_log";	my $query_handle = $connect->prepare($query);	$query_handle->execute();	$query_handle->bind_columns(\my $average_down_KBs);	$query_handle->fetch();
	
	if ($debug) {print "debug:   query=$query\n";}
	if ($debug) {print "debug:   average_down_KBs = $average_down_KBs\n";}
	
	return $average_down_KBs;
}


sub sub_get_maxKBs_from_mysql() {
	if ($debug) {print "debug: entering sub_get_maxKBs_from_mysql()\n";}
	my $connect = DBI->connect($dsn, $user, $pw);

	# SELECT MAX KBs as $max_kbs_KBs
	my $query = "SELECT ts, max(KBs) FROM manual_log";	my $query_handle = $connect->prepare($query);	$query_handle->execute();	$query_handle->bind_columns(\my $max_kbs_ts, \my $max_kbs_KBs);	$query_handle->fetch();
	
	if ($debug) {print "debug:   query=$query\n";}
	if ($debug) {print "debug:   max_kbs_KBs = $max_kbs_KBs\n";}

	return $max_kbs_KBs;
}

sub sub_get_record_count_from_mysql() {
	if ($debug) {print "debug: entering sub_get_number_of_records()\n";}
	my $connect = DBI->connect($dsn, $user, $pw);

	# SELECT MAX KBs as $max_kbs_KBs
	my $query = "SELECT count(*) FROM manual_log";	my $query_handle = $connect->prepare($query);	$query_handle->execute();	$query_handle->bind_columns(\my $record_count);	$query_handle->fetch();
	
	if ($debug) {print "debug:   query=$query\n";}
	if ($debug) {print "debug:   record_count = $record_count\n";}

	return $record_count;
}

sub sub_get_resetslog_from_mysql() {
	if ($debug) {print "debug: entering sub_get_resetslog_from_mysql()\n";}

	my $return=`mysql -u $user --password=$pw --raw $database -e "select * from manual_log where resetted = 'true' order by id;"`;	
	#my $return=`mysql -u $user --password=$pw --raw $database -e "select * from manual_log order by id;"`;	
	print $return;

	exit;
}

sub sub_get_fulllog_from_mysql() {
	if ($debug) {print "debug: entering sub_get_fulllog_from_mysql()\n";}

	my $return=`mysql -u $user --password=$pw --raw $database -e "select id, ts, bps_down, bps_up, KBs, uptime, resetted from manual_log order by id;"`;	
	
	print $return;

	exit;
}

sub sub_backup_database() {
	if ($debug) {print "debug: entering sub_backup_database()\n";}
	
	sub_ensure_you_are_root();

	my $return=`/usr/local/scripts/backups/adsldb-mysql-backup.sh`;	
	print $return;

	exit;
}

sub sub_reset_adsl_connection() {
	if ($debug) {print "debug: entering sub_reset_adsl_connection()\n";}

	# this has been tested and works:
	# $ curl --silent --user admin:o7n2p3UsMV -F "factory=E0" http://router/reset/
	# note that "factory=E1" is a factory reset.. ;)
	
	print "ALERT!! An ADSL reset will terminate this and all other sessions!!";
	if (sub_verify_to_proceed() eq "verified") {	
		print "\nADSL resetting now. Goodbye!\n\n";
		if ($debug) {print "debug: print 'curl --silent --user admin:o7n2p3UsMV -F 'factory=E0' http://router/reset/'\n";}
		`curl --silent --user admin:o7n2p3UsMV -F "factory=E0" http://router/reset/`;
	}
}

sub sub_get_previous_KBs() {
	if ($debug) {print "debug: entering sub_get_previous_KBs()\n";}
	
	my $connect = DBI->connect($dsn, $user, $pw);
	
	# GET THE TOTAL NUMBER OF ROWS
	my $query = "SELECT COUNT(*) FROM manual_log";	my $query_handle = $connect->prepare($query);	$query_handle->execute();	$query_handle->bind_columns(\my $row_count);	$query_handle->fetch();
	
	# SET THE RETURN LIMIT SO WE GET THE 2nd LAST ENTRY
	my $row_return_limit = 1;
	my $row_return_start = $row_count - $row_return_limit;

	# SELECT THE LAST X ROWS FROM THE END
	my $query = "SELECT KBs FROM manual_log ORDER BY id LIMIT $row_return_start, $row_return_limit";	my $query_handle = $connect->prepare($query);	$query_handle->execute();	$query_handle->bind_columns(\my $previous_KBs);	$query_handle->fetch();	
	
	if ($debug) {print "debug:   query=$query\n";}	
	if ($debug) {print "debug:   previous_KBs=$previous_KBs\n";}
		
	return $previous_KBs;
}

sub sub_get_previous_uptime() {
	if ($debug) {print "debug: entering sub_get_previous_uptime()\n";}
	
	my $connect = DBI->connect($dsn, $user, $pw);
	
	# GET THE TOTAL NUMBER OF ROWS
	my $query = "SELECT COUNT(*) FROM manual_log";
	my $query_handle = $connect->prepare($query);
	$query_handle->execute();
	$query_handle->bind_columns(\my $row_count);
	$query_handle->fetch();
	
	# SET THE RETURN LIMIT SO WE GET THE 2nd LAST ENTRY
	my $row_return_limit = 1;
	my $row_return_start = $row_count - $row_return_limit;

	# SELECT THE LAST X ROWS FROM THE END
	my $query = "SELECT uptime FROM manual_log ORDER BY id LIMIT $row_return_start, $row_return_limit";
	my $query_handle = $connect->prepare($query);
	$query_handle->execute();
	$query_handle->bind_columns(\my $previous_uptime_in_seconds);
	$query_handle->fetch();	
	
	if ($debug) {print "debug:   sub_get_previous_uptime(); previous_uptime_in_seconds=$previous_uptime_in_seconds\n";}
		
	return $previous_uptime_in_seconds;
}

sub sub_detect_reset_event($){
	local(my $connection_uptime_in_seconds=$_[0]);
	if ($debug) {print "debug: entering sub_detect_reset_event($connection_uptime_in_seconds)\n";}
		
	my $connect = DBI->connect($dsn, $user, $pw);
	
	#~ # GET THE TOTAL NUMBER OF ROWS and put it in $row_count
	#~ my $query = "SELECT COUNT(*) FROM manual_log";	my $query_handle = $connect->prepare($query);	$query_handle->execute();	$query_handle->bind_columns(\my $row_count);	$query_handle->fetch();
	#~ # SET THE RETURN LIMIT SO WE GET THE 2nd LAST ENTRY
	#~ my $row_return_limit = 1;	my $row_return_start = $row_count - $row_return_limit;
	#~ # SELECT THE LAST X ROWS FROM THE END
	#~ my $query = "SELECT uptime FROM manual_log ORDER BY id LIMIT $row_return_start, $row_return_limit";	my $query_handle = $connect->prepare($query);	$query_handle->execute();	$query_handle->bind_columns(\my $previous_uptime_in_seconds);	$query_handle->fetch();	
	
	my $previous_uptime_in_seconds=sub_get_previous_uptime();
	
	if ($debug) {print "debug:   sub_detect_reset_event(): previous_uptime_in_seconds=$previous_uptime_in_seconds\n";}
	if ($debug) {print "debug:   sub_detect_reset_event(): connection_uptime_in_seconds=$connection_uptime_in_seconds\n";}
	
	my $reset_event_detected="";	
	#if ($previous_uptime_in_seconds < $connection_uptime_in_seconds) { # <- USE THIS TO FORCE RESET EVENT
	if ($previous_uptime_in_seconds > $connection_uptime_in_seconds) {
		$reset_event_detected="true";
		if ($debug) {print "debug:   setting reset_event_detected=true\n"; }
	} else {
		if ($debug) {print "debug:   reset_event_detected=$reset_event_detected\n";}
	}
	
	return $reset_event_detected;
}

sub sub_adsl_stats() {
	if ($debug) {print "debug: entering sub_adsl_stats()\n";}
	# GRAB THE STATS FROM THE MODEM AS A LARGE STRING
	my $return=`curl --silent --user admin:o7n2p3UsMV http://router/configuration/adsl.html?ImPorts.a1 | egrep -A 1 'Upstream|Downstream|Elaspsed|Annex Type'`;
	chomp($return);	my $hs = HTML::Strip->new(); my $clean_text = $hs->parse( $return );	my @clean_text_split = split (/\s+/,$clean_text);
	
	# PARSE DATA OUT OF THE $clean_text_split ARRAY
	my $connection_uptime_string= join(" ",(@clean_text_split[12..19]));
	my ($day,$hr,$min,$sec) = ($connection_uptime_string =~ m/(\d+)/gi); 
	my $connection_uptime_in_seconds = (($day*3600*24) + ($hr * 3600) + ($min * 60) + $sec); 
	my $connection_uptime_in_minutes = ceil($connection_uptime_in_seconds/60);
	my $adsl_annex_type=$clean_text_split[2]; 
	my $down_bits_per_second=$clean_text_split[8]; my $up_bits_per_second=$clean_text_split[5];
	my $down_kilobits_per_second=ceil($down_bits_per_second/1000); my $up_kilobits_per_second=ceil($up_bits_per_second/1000);
	my $down_megabits_per_second=nearest(.5,$down_bits_per_second/1000/1000); my $up_megabits_per_second=nearest(.5,$up_bits_per_second/1000/1000);
	my $down_kilobytes_per_second=ceil($down_bits_per_second/8/1024); my $up_kilobytes_per_second=ceil($up_bits_per_second/8/1024);
	my $down_megabytes_per_second=nearest(.05,$down_kilobytes_per_second/1024); my $up_megabytes_per_second=nearest(.05,$up_kilobytes_per_second/1024);
	
	# PULL STATS FROM MYSQL
	my $rounding_threshold=.9;
	
	my $average_down_KBs=ceil(sub_get_average_speed_from_mysql());
	my $rounded_average_down_KBs=$average_down_KBs*$rounding_threshold;
	my $max_down_KBs=sub_get_maxKBs_from_mysql(); 
	my $max_uptime_in_seconds=sub_get_maxuptime_from_mysql(); my $max_uptime_in_mins = ceil($max_uptime_in_seconds/60);		
	my $avg_uptime_in_seconds = sub_get_average_uptime_from_mysql(); my $avg_uptime_in_mins = ceil($avg_uptime_in_seconds/60);
	my $rounded_avg_uptime_in_mins=$avg_uptime_in_mins*$rounding_threshold;
	my $previous_uptime_in_seconds=sub_get_previous_uptime();
	my $previous_KBs=sub_get_previous_KBs();
	my $reset_event_detected=sub_detect_reset_event($connection_uptime_in_seconds);	
	my $record_count=sub_get_record_count_from_mysql();
			
	# INSERT THE NEW DATA INTO MYSQL 
	sub_insert_new_stats_into_mysql($down_bits_per_second,$up_bits_per_second,$down_kilobytes_per_second,$connection_uptime_in_seconds,$reset_event_detected,$previous_uptime_in_seconds,$previous_KBs);
	
	#~ # INSERT THE NEW DATA INTO MYSQL BUT ONLY IF $previous_uptime_in_seconds > X
	#~ #   the theory being that will prevent logging multiple 0s as uptime
	#~ #   which throws out the reset detection algoritm if ($previous_uptime_in_seconds > $connection_uptime_in_seconds) {$reset=1}
	#~ #   and also fills our lovely database with lots of 0 entries which are useless in every sense.
	#~ if ($previous_uptime_in_seconds >= 60) {
		#~ sub_insert_new_stats_into_mysql($down_bits_per_second,$up_bits_per_second,$down_kilobytes_per_second,$connection_uptime_in_seconds,$reset_event_detected,$previous_uptime_in_seconds,$previous_KBs);
	#~ } else {
		#~ if ($debug) {print "debug: not insserting new stats because $previous_uptime_in_seconds > 0\n";}
	#~ }
	
	if (($verbose)) { # PRINT LOTS OF EXTRA STUFF
		print "verbose, adsl: clean_text_split = @clean_text_split\n\n";
		print "verbose, adsl: adsl_annex_type = $adsl_annex_type\n";
		print "verbose, adsl: connection_uptime_in_seconds = $connection_uptime_in_seconds\n";
		print "verbose, adsl: down_bits_per_second / up_bits_per_second (bps) = $down_bits_per_second/$up_bits_per_second\n";
		print "verbose, adsl: down_kilobits_per_second / up_kilobits_per_second (kbs) = $down_kilobits_per_second/$up_kilobits_per_second\n";
		print "verbose, adsl: down_megabits_per_second / up_megabits_per_second (mbs) ~ $down_megabits_per_second/$up_megabits_per_second\n";
		print "verbose, adsl: down_kilobytes_per_second / up_kilobytes_per_second (KB/s) ~ $down_kilobytes_per_second/$up_kilobytes_per_second\n\n";
		print "verbose, mysql: average_down_KBs = $average_down_KBs\n";
		print "verbose, mysql: max_down_KBs = $max_down_KBs\n";
		print "verbose, mysql: max_uptime_in_seconds = $max_uptime_in_seconds\n";
		print "verbose, mysql: avg_uptime_in_mins = $avg_uptime_in_mins\n";
		print "verbose, mysql: previous_KBs = $previous_KBs\n";
		print "verbose, mysql: rounding_threshold = $rounding_threshold\n";		 
		print "verbose, mysql: rounded_average_down_KBs=$rounded_average_down_KBs\n";
		print "verbose, mysql: rounded_avg_uptime_in_mins=$rounded_avg_uptime_in_mins\n";
		print "verbose, mysql: record_count=$record_count\n\n";		
		
		#print "verbose, mysql: previous_uptime_in_seconds = $previous_uptime_in_seconds\n";
	}
	 
	# ACTUALLY REPORT WHATS HAPPENING TO THE SCREEN, WARNING, THIS CODE IS EXACTING.
	if(!$silent){
		# ADSL TYPE
		#print "$adsl_annex_type synced @ $down_kilobits_per_second/$up_kilobits_per_second"."kbs ~ ";
                print "$adsl_annex_type synced @~ ";
		
		# ADSL2 synced @ 5614/1011kbs ~ 686KB/s (avg 707, max 738 NR!). 
		#  if we're faster than average, print the KBs in green, else in red.
		if ($down_kilobytes_per_second < $rounded_average_down_KBs) {print colored ("$down_kilobytes_per_second"."KB/s",'red');} 
			else {print colored("$down_kilobytes_per_second"."KB/s",'green');}
		print " (avg $average_down_KBs, max $max_down_KBs"; 
		#  if current speed = max then print New Record!! :)
		if ($down_kilobytes_per_second >= $max_down_KBs) {print colored (" NR!",'green');} print ")."; 
						
		# Up 182 mins (avg 1985, max 4969); 0 day 3 hr 1 min 53 sec. 
		print " Up ";		
		#  if we're up less than average, print the mins in red, else in green.
		if ($connection_uptime_in_minutes < $rounded_avg_uptime_in_mins) {print colored ("$connection_uptime_in_minutes mins",'red');} 
			else {print colored("$connection_uptime_in_minutes mins",'green');}
		#  if current uptime > max then print New Record!! :)
		if ($connection_uptime_in_minutes > $max_uptime_in_mins) {print colored (" NR!",'green');} 
		print" (avg $avg_uptime_in_mins, max $max_uptime_in_mins); $connection_uptime_string.\n";
				
		if ($reset_event_detected eq "true") {print "\nALERT!! $adsl_annex_type Reset event detected!!\n";}
	}
}

sub sub_verify_to_proceed() {
	if ($debug) {print "debug: entering sub_adsl_stats()\n";}

    use String::Random;
    my $foo = new String::Random;
    #my $random_string = $foo->randpattern("CcCnCn");
    my $random_string = $foo->randpattern("Cn");
    #my $random_string=ASDFASD;
        
    print " ..enter $random_string to verify:  ";
    chomp(my $user_input = <STDIN>);
    if ($user_input eq $random_string) {
        if ($debug) {print "  debug: verification passed\n";}
        return "verified";
    } else {
        print "\nERROR: Verification failed, exiting\n";
        exit;           
    }
}

sub sub_ensure_you_are_root() {
	# ensure this is being run as root
	my $whoami = getpwuid($>);
	if ($whoami ne "root") {
       print "This command needs to be run as root (use sudo)\n";
       exit;
	}
}

sub sub_stats_this_script() {
	if ($debug) {print "debug: entering sub_script_stats\n"; }
	
	# basically i just want this to say the name of the script, which apparently $0 has
	my $script_name = $0;		
	
	# but $0 actually returns a script in the form ./peer2peer_dev.sh, and i don't want the ./ at the start
	#   there are two ways to go about fixing that, the regex fix is "$script_name =~ s/\W+//g;"
	#   but we're supposed to use a module if one exists, and one does:
	$script_name = File::Spec-> abs2rel ( $script_name ) ;	
	
	# that produces a nice neat peer2peer_dev.sh.; then i'd like to get the parent path
	#   for the script, so we can parse it for a .svn folder in a sec
	my $full_path_name = File::Spec-> rel2abs ( $script_name ) ;	
	(my $volume,my $script_parent_path,my $file) = File::Spec->splitpath( $full_path_name );
			
	# we grep .svn/entries to see if there is a svn revision for this script
	my $command_return = `grep -A 2 $script_name $script_parent_path/.svn/entries | tail -1`;
	my @command_return_split = split (/\s+/,$command_return); 
	my $svn_revision = $command_return_split[0];
	
	if ($debug)   {print "debug:   now running $script_name, svn.rev $svn_revision\n"; }		
	if ($verbose) {print "verbose:   script_name: $script_name\n"; }
	if ($verbose) {print "verbose:   script_parent_path: $script_parent_path\n"; }
	if ($verbose) {print "verbose:   svn_revision: $svn_revision\n"; }	
}
