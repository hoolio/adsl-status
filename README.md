adsl-status
===========

 adsl-status - Julius Roberts (hooliowobbits@gmail.com) 
 
  Copyright 2009, 2012 by Julius Roberts
  Some rights reserved. See COPYING

A monolithic perl script to interrogate an ADSL router (via curl).
Writes back to MySQL and reports averages/peaks etc.
I'm using an Eagle Networks Digital Link DL710 ADSL2+ router
It won't work for yours unless you change the router url, password and whatnot

ABOUT:
connect to our ADSL router with curl and pull down the connection stats
strip out the html etc and make the data into nice perl variables
pull max uptime, maxspeed, average speed from sql
detect adsl reset events (current_uptime < previous_uptime)
compare current stats with maximum stats from SQL to alert if we have a new record!
squirt all the newly gathered data into mysql
finally print out some useful stats
plus:
if run with --reset connect to our ADSL router with curl and reset the adsl connection
if run with --log show the last 5 rows, or if --log X, show the last X rows.
if run with --verbose run as per --log plus some more detailed info on speed etc.
if run with --debug then output lots of intermediary debug crap blah etc

$ ./adsl-status.sh --help
usage: adsl.sync[.dev].sh [--verbose|--silent|--help|--debug] | [--reset]

$ ./adsl-status.sh --verbose
verbose, adsl: clean_text_split = Annex Type ADSL2 -- Upstream 967600 -- Downstream 5374560 -- Elaspsed Time 16 day 19 hr 21 min 11 sec

verbose, adsl: adsl_annex_type = ADSL2
verbose, adsl: connection_uptime_in_seconds = 1452071
verbose, adsl: down_bits_per_second / up_bits_per_second (bps) = 5374560/967600
verbose, adsl: down_kilobits_per_second / up_kilobits_per_second (kbs) = 5375/968
verbose, adsl: down_megabits_per_second / up_megabits_per_second (mbs) ~ 5.5/1
verbose, adsl: down_kilobytes_per_second / up_kilobytes_per_second (KB/s) ~ 657/119

verbose, mysql: average_down_KBs = 662
verbose, mysql: max_down_KBs = 747
verbose, mysql: max_uptime_in_seconds = 5360432
verbose, mysql: avg_uptime_in_mins = 8390
verbose, mysql: previous_KBs = 657
verbose, mysql: rounding_threshold = 0.9
verbose, mysql: rounded_average_down_KBs=595.8
verbose, mysql: rounded_avg_uptime_in_mins=7551
verbose, mysql: record_count=27086

ADSL2 synced @~ 657KB/s (avg 662, max 747). Up 24202 mins (avg 8390, max 89341); 16 day 19 hr 21 min 11 sec.

